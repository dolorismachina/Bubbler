
extends Node2D

const TOP = 0
const BOTTOM = 1
const LEFT = 2
const RIGHT = 3

const COLOR_BLUE = 0
const COLOR_RED = 1
const COLOR_GREEN = 2
const COLOR_YELLOW = 3
const COLOR_NONE = 99999999

var bubbleBlue = preload("res://blue.png")
var bubbleGreen = preload("res://red.png")
var bubbleYellow = preload("res://green.png")
var bubbleRed = preload("res://yellow.png")

var cannon
var grid

var bubbles = [] # Array of three bubbles. One current and two next.
var preview1
var preview2
var turn = 0

func _ready():
	print(get_node("/root/Bubbler"))
	set_process_input(true)

	cannon = get_node("Cannon")
	grid = get_node("Grid")

	preview1 = get_node("Cannon/Bubble1")
	preview2 = get_node("Cannon/Bubble2")

	var newPos = Vector2(get_viewport_rect().size.x * 0.5 - grid.cellSize * 0.5, get_viewport_rect().size.y - 50)
	cannon.set_pos(newPos)
	cannon.set_rot(deg2rad(180))
	cannon.currentCell = Vector2(floor(grid.gridSize * 0.5) - 1, 0)
	cannon.currentDir = self.BOTTOM

	# Create array of bubbles.
	for i in range(0, 3):
		bubbles.append(randomBubble())

	cannon.loadCannon(bubbles[0])


func nextTurn():
	if grid.cellsAvailable > 0:
		placeCannon()
		setupBubbles()
		cannon.loadCannon(bubbles[0])
		updatePreview()

	else:
		print("Game Over")

	turn += 1


func placeCannon():
	randomize()
	var rnd = floor(rand_range(0, 4))
	rnd = self.BOTTOM

	var cellNumberIsEven = grid.gridSize % 2 == 0

	while grid.isEdgeAvailable(rnd) == false:
		rnd = floor(rand_range(0, 4))

	if rnd == self.TOP:
		# Top
		var newPos = Vector2(get_viewport_rect().size.x * 0.5 - grid.cellSize * 0.5, 50)
		cannon.set_pos(newPos)
		cannon.set_rot(0)
		cannon.currentCell = Vector2(floor(grid.gridSize * 0.5) - 1, 0)
		cannon.currentDir = self.TOP

	elif rnd == self.RIGHT:
		# Right
		var newPos = Vector2(get_viewport_rect().size.x - 50, get_viewport_rect().size.y * 0.5 - grid.cellSize * 0.5)
		cannon.set_pos(newPos)
		cannon.set_rot(deg2rad(270))
		cannon.currentCell = Vector2(0, floor(grid.gridSize * 0.5) - 1)
		cannon.currentDir = self.RIGHT

	elif rnd == self.BOTTOM:
		# Bottom
		var newPos
		if cellNumberIsEven:
			newPos = Vector2(get_viewport_rect().size.x * 0.5 - grid.cellSize * 0.5, get_viewport_rect().size.y - 50)
			cannon.currentCell = Vector2(floor(grid.gridSize * 0.5) - 1, 0)

		else:
			newPos = Vector2(get_viewport_rect().size.x * 0.5, get_viewport_rect().size.y - 50)
			cannon.currentCell = Vector2(floor(grid.gridSize * 0.5), 0)

		cannon.set_pos(newPos)
		cannon.set_rot(deg2rad(180))
		cannon.currentDir = self.BOTTOM

	else:
		# Left
		var newPos = Vector2(50, get_viewport_rect().size.y * 0.5 - grid.cellSize * 0.5)
		cannon.set_pos(newPos)
		cannon.set_rot(deg2rad(90))
		cannon.currentCell = Vector2(0, floor(grid.gridSize * 0.5) - 1)
		cannon.currentDir = self.LEFT


func setupBubbles():
	bubbles[0] = bubbles[1]
	bubbles[1] = bubbles[2]
	bubbles[2] = randomBubble()


func randomBubble():
	var rnd = floor(rand_range(0, 4))

	if rnd == COLOR_BLUE:
		return COLOR_BLUE
	elif rnd == COLOR_RED:
		return COLOR_RED
	elif rnd == COLOR_GREEN:
		return COLOR_GREEN
	elif rnd == COLOR_YELLOW:
		return COLOR_YELLOW

	return null


func bubbleTexture(color):
	var bubble = get_node("Cannon/Bubble")

	if color == COLOR_BLUE:
		return bubbleBlue

	elif color == COLOR_RED:
		return bubbleRed

	elif color == COLOR_GREEN:
		return bubbleGreen

	elif color == COLOR_YELLOW:
		return bubbleYellow


func updatePreview():
	preview1.set_texture(bubbleTexture(bubbles[1]))
	preview2.set_texture(bubbleTexture(bubbles[2]))
