
extends Sprite

var currentCell # Where in the grid is the cannon located.
var currentDir
var currentColor

var grid # Reference to t he grid holding the bubbles.
var game # Reference to the main game script.


func _ready():
	game = get_parent()
	set_process_input(true)

	self.grid = get_parent().get_node("Grid")
	self.game = get_parent()

func _input(event):
	if event.type == InputEvent.KEY and event.is_pressed() and not event.is_echo():
		var rotDeg = floor(rad2deg(get_rot()))

		if rotDeg == 0 or rotDeg == 180:
			if event.scancode == KEY_LEFT:
				move(-1, 0)
			elif event.scancode == KEY_RIGHT:
				move(1, 0)
		elif rotDeg == 90 or rotDeg == 270:
			if event.scancode == KEY_UP:
				move(0, -1)
			elif event.scancode == KEY_DOWN:
				move(0, 1)

		if event.scancode == KEY_SPACE:
			shoot()
			game.nextTurn()

func move(x, y):
	# Left
	if x < 0 and currentCell.x > 0:
		var newPos = Vector2(get_pos().x + grid.cellSize * x, get_pos().y)
		self.set_pos(newPos)
		currentCell.x += x
	# Right
	if x > 0 and currentCell.x < grid.gridSize - 1:
		var newPos = Vector2(get_pos().x + grid.cellSize * x, get_pos().y)
		self.set_pos(newPos)
		currentCell.x += x
	elif y < 0 and currentCell.y > 0:
		# Up
		var newPos = Vector2(get_pos().x, get_pos().y + grid.cellSize * y)
		self.set_pos(newPos)
		currentCell.y += y
	elif y > 0 and currentCell.y < grid.gridSize - 1:
		# Down
		print("down")
		var newPos = Vector2(get_pos().x, get_pos().y + grid.cellSize * y)
		self.set_pos(newPos)
		currentCell.y += y


func shoot():
	var color = ""
	
	if currentColor == game.COLOR_BLUE:
		color = "blue"
	elif currentColor == game.COLOR_RED:
		color = "red"
	elif currentColor == game.COLOR_GREEN:
		color = "green"
	elif currentColor == game.COLOR_YELLOW:
		color = "yellow"
	var targetCell

	if currentDir == game.TOP:
		if grid.grid[currentCell.x][0] == "blank":
			# Run through all available cells.
			for i in range(grid.gridSize):
				if grid.grid[currentCell.x][i] == "blank":
					targetCell = Vector2(currentCell.x, i)
				else:
					break
			grid.insertBall(targetCell, color)
		else:
			print("blocked line")

	elif currentDir == game.BOTTOM:
		if grid.grid[currentCell.x][grid.gridSize - 1] == null:
			# Run through all available cells.
			for i in range(grid.gridSize - 1, -1, -1):
				if grid.grid[currentCell.x][i] == null:
					targetCell = Vector2(currentCell.x, i)
				else:
					break
			grid.insertBall(targetCell, currentColor)
		else:
			print("blocked line")

	elif currentDir == game.LEFT:
		print("CHeck left")
		if grid.grid[0][currentCell.y] == "blank":
			# Run through all available cells.
			for i in range(grid.gridSize):
				if grid.grid[i][currentCell.y] == "blank":
					targetCell = Vector2(i, currentCell.y)
				else:
					break
			grid.insertBall(targetCell, currentColor)
		else:
			print("blocked line")

	elif currentDir == game.RIGHT:
		if grid.grid[grid.gridSize - 1][currentCell.y] == "blank":
			# Run through all available cells.
			for i in range(grid.gridSize - 1, -1, -1):
				if grid.grid[i][currentCell.y] == "blank":
					targetCell = Vector2(i, currentCell.y)
				else:
					break
			grid.insertBall(targetCell, currentColor)
		else:
			print("blocked line")

	game.nextTurn()


func loadCannon(color):
	currentColor = color

	var bubble = get_node("Bubble0")

	bubble.set_texture(game.bubbleTexture(color))
