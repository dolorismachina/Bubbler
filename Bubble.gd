
extends Sprite

var color

var game

func _ready():
	game = get_node("/root/Bubbler")
	color = game.COLOR_NONE


func init(color, position):
	set_texture(game.bubbleTexture(color))
	set_pos(position)
	set_centered(false)
	
	self.color = color