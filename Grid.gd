
extends Node2D

var game

var bubbleScene = preload("res://Bubble.scn")

var grid = []
var cellSize = 64
var gridSize = 5

var cellTexture = preload("res://grid.png")

var cellsAvailable = gridSize * gridSize

# Grid centre:
func _ready():
	game = get_parent()

	for x in range(gridSize):
		grid.append([])

		for y in range(gridSize):
			grid[x].append("")

			var pos = Vector2(x * cellSize - gridSize * cellSize * 0.5, y * cellSize - gridSize * cellSize * 0.5)

			var cell = Sprite.new()
			cell.set_texture(cellTexture)
			cell.set_pos(pos)
			cell.set_centered(false)
			self.add_child(cell)

			grid[x][y] = null


func insertBall(pos, color):
	var cell = bubbleScene.instance()
	var spritePos = Vector2(pos.x * cellSize - gridSize * cellSize * 0.5, pos.y * cellSize - gridSize * cellSize * 0.5)
	self.add_child(cell)
	
	cell.init(color, spritePos)
	

	grid[pos.x][pos.y] = cell

	cellsAvailable -= 1
	
	matchExists(pos.x, pos.y)

func matchExists(x, y):
	var matches = 1
	var lastMatch # Last cell that matches a color. First matched is this - (number of needed cells to match - 1)
	var firstMatch
	
	# Check horizontally.
	for i in range(1, gridSize):
		if grid[i][y] != null and grid[i - 1][y] != null:
			if grid[i][y].color == grid[i - 1][y].color:
				matches += 1
				lastMatch = i
			elif grid[i][y] == null:
				matches = 1
		else:
			matches = 1
	
	if matches >= 3: # Fix this so it adapts to different scenarios and removes all needed bubbles.
		firstMatch = lastMatch - 2
		cellsAvailable += 3
		
		# Clear the row
		for col in range(firstMatch, lastMatch + 1):
			print("clearing col: " + str(col) + " and row: " + str(y))
			grid[col][y].free()
			grid[col][y] = null
		
		# Move up lower rows
		for row in range(y + 1, gridSize):
			moveRowUp(row, firstMatch, lastMatch + 1)
		

func moveRowUp(row, start, end):
	for col in range(start, end):
		if grid[col][row] == null:
			grid[col][row - 1] = null
		else:
			# Copy cell
			var cell = bubbleScene.instance()
			self.add_child(cell)
			
			var cellX = grid[col][row].get_pos().x
			var cellY = grid[col][row].get_pos().y - cellSize
			var newPos = Vector2(cellX, cellY)
			
			cell.init(grid[col][row].color, newPos)
		
			# Move the cell up
			grid[col][row - 1] = cell
			grid[col][row].free()
			grid[col][row] = null


func isEdgeAvailable(edge):
	#if edge == game.TOP:
	#	for i in range(0, gridSize):
	#		if grid[i][0] == "blank":
	#			return true

	#	return false

	if edge == game.BOTTOM:
		for i in range(0, gridSize):
			if grid[i][gridSize - 1] == null:
				return true

		return false

	#elif edge == game.LEFT:
	#	for i in range(0, gridSize):
	#		if grid[0][i] == "blank":
	#			return true

	#		return false

	#elif edge == game.RIGHT:
	#	for i in range(0, gridSize):
	#		if grid[gridSize - 1][i] == "blank":
	#			return true

	#	return false

	#else:
	#	print("Unknown input.")
